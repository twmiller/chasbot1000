# bot.py
import os
import random
import operator
import json
import requests
from pprint import pprint
from uncertainties import ufloat
from discord.ext import commands
from dotenv import load_dotenv
from peewee import *

load_dotenv()
token = os.getenv('DISCORD_TOKEN')
guild = os.getenv('DISCORD_GUILD')
version = os.getenv('CHASBOT_VERSION')
weather_api_key = os.getenv('OPENWEATHER_API')
bot = commands.Bot(command_prefix='c!')

db = SqliteDatabase('chas_status.db')

class ChasStat(Model):
    status = CharField()

    class Meta:
        database = db # This model uses the "people.db" database.

@bot.command(name='wstatus', help='Write a ChasBot status to the database.')
async def save_status(ctx, *, status: str):
    new_status = ChasStat(status=status)
    new_status.save()
    await ctx.send('New status added: ' + status)

@bot.command(name='status', help='Returns the current status of The ChasBot1000')
async def on_message(ctx):
    random_query = ChasStat.select().order_by(fn.Random())
    status_obj = random_query.get()
    this_status = 'ChasBot1000 Mark I\nCurrent version: ' + version + '\nCurrent status: ' + status_obj.status
    await ctx.send(this_status)

@bot.command(name='roll', help='Simulates rolling dice: e.g. 2d6')
async def roll(ctx, dice: str):
    chas_dice = [
        'You have lost your dice.',
        'Critical miss: your arm falls off.',
        'Critical hit: your arm falls on.',
        'The dice rolled under the table.',
        'My, what a yummy mango.',
        'Looks like a...yep...that\'s definitely a \'1\'.',
        'The dice vanished in a puff of smoke.',
        'You rolled a ' + str(random.choice(range(128, 256))) + '.',
        'It\'s either ' + str(random.choice(range(1, 13)))
            + ' or ' + str(random.choice(range(13, 24))) + '...  Can\'t really tell.',
        'Definitely not a ' + str(random.choice(range(1, 13))) + '.',
        'Looks like a crit....hit or miss.  You decide.',
        'You try to roll the dice, but your hand/eye coordination fails you.'
    ]
    response = random.choice(chas_dice)
    await ctx.send(response)

@bot.command(name='calc', help='Advanced blockchain calculations: e.g. 22 / 7')
async def roll(ctx, first_number: float, operation: str, second_number: float):
    correct = 0
    if operation == '+':
        correct = first_number + second_number
    elif operation == '-':
        correct = first_number - second_number
    elif operation == '/':
        correct = first_number / second_number
    elif operation == '*':
        correct = first_number * second_number
    get_max_percent = random.choice(range(1, 26))
    get_percent = random.choice(range(1, get_max_percent))
    percentage = correct * (get_percent / 100)
    operators = {
        '+': operator.add, 
        '-': operator.sub,
    }
    keys = list(operators)
    random_calc = random.choice(keys)
    adjusted = operators[random_calc](correct, percentage)
    incorrect = ufloat(adjusted, (get_max_percent / 100))
    await ctx.send('I calculate ' 
            + str(first_number) + ' ' + operation + ' '  
            + str(second_number) + ' to be ' 
            + str(incorrect) + '.')

@bot.command(name='weather', help='When provided with a valid zipcode, that zipcode will be ignored and the current weather for a different location will be returned.')
async def weather(ctx, zipcode: int):
    valid_zips = {}
    with open('valid-zips.json') as vz:
        valid_zips = json.load(vz)
    if (zipcode < 10000 or zipcode > 99999):
        message = 'What kind of a fool do you take me for? That\'s not a *real* zipcode.'
    else:
        random_index = random.randint(0, len(valid_zips)-1)
        weatherURL = 'http://api.openweathermap.org/data/2.5/weather?zip=' + str(valid_zips[random_index]) + ',us&units=imperial&APPID=' + weather_api_key 
        resp = requests.get(url=weatherURL)
        data = resp.json() 
        message = 'AccuChas Weather Report for:  ' + str(zipcode) + '\nName: ' + data['name'] + '\nConditions: ' + data['weather'][0]['description'] + '\nTemperature: ' + str(data['main']['temp_max']) + 'F'        
    await ctx.send(message)

db.connect()
db.create_tables([ChasStat])
bot.run(token)
