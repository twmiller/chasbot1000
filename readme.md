# ChasBot 1000

A shenangigans / hijinks bot for discord, based off of original organic proptotype, ChasBot1000 is an artificial chaos construct designed to annoy by providing largely useless information.

Discord command: c!

## Supported functions:

### _calc_

Solve a mathmatical problem within a randomly generated margin of error less that 25%.  While not particularly useful, it's not (technically) wrong.

variables: first number (integer), operator, second number (integer)
example: 
> c!calc 100 + 100

### _status_

Retrieves the current version number and a random quote from the ChasBot status database. Version number is low enough to be useless, status has no bearing on reality.

variables: none

example: 
> c!status

### _wstatus_

Writes a new status to the ChasBot status database, used by the _c!status_ command. This one actually does what it's supposed to.

variables: status (string)

example: 
> c!wstatus This is a new status for the ChasBot1000.

### _roll_

Generates a random message that may or may not contain information related to a dice roll. You can provide the sides and number of dice you want to roll, but that will almost certainly be ignored.

variables: _n_d_s_ where n is the number of dice to roll and s is the number of sides per dice.

example: 
> c!roll 1d6

### _weather_

When provided with a valid zipcode, that zipcode will be ignored and the current weather for a different location will be returned.

variables: standard American 5-digit zip code

example;
> c!weather 80526
